<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\HomeController;
use App\Http\Controllers\AboutController;
use App\Http\Controllers\PortfolioController;
use App\Http\Controllers\ContactController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', [HomeController::class,'index'] );
Route::get('/about', [AboutController::class,'about'] );
Route::get('/portfolio', [PortfolioController::class,'portfolio']);
Route::get('/contact', [ContactController::class,'contact']);


/*Route::get('/', function () {
    return view('index',[
        "title" => "Home",
    ]);
});

Route::get('/portfolio', function () {
    return view('portfolio',[
        "title" => "Portfolio",
    ]);
});

Route::get('/blog', function () {
    return view('blog',[
        "title" => "Blog",
    ]);
});

Route::get('/blog-post', function () {
    return view('blog-post',[
        "title" => "Post",
    ]);
});

Route::get('/contact', function () {
    return view('contact',[
        "title" => "Contact",
    ]);
});

Route::get('/about', function () {
    return view('about',[
        "title" => "About Me",
    ]);
});*/
