﻿@extends('layout')

@section('konten')
<body class="home">
    <!-- Main Content Starts -->
    <section class="container-fluid main-container container-home p-0 revealator-slideup revealator-once revealator-delay1">
        <div class="color-block d-none d-lg-block"></div>
        <div class="row home-details-container align-items-center">
            <div class="col-lg-4 bg position-fixed d-none d-lg-block"></div>
            <div class="col-12 col-lg-8 offset-lg-4 home-details text-left text-sm-center text-lg-left">
                <div>
                    <img src="/img/img-mobile.jpg" class="img-fluid main-img-mobile d-none d-sm-block d-lg-none" alt="AnggaPermana" />
                    <h6 class="text-uppercase open-sans-font mb-0 d-block d-sm-none d-lg-block">Halo!</h6>
                    <h1 class="text-uppercase poppins-font"><span>Perkenalkan, saya</span> Angga Permana</h1>
                    <p class="open-sans-font">
                        Mahasiswa dari Universitas Pendidikan Ganesha, program studi Pendidikan Teknik Informatika. Berpengalaman
                        magang di bidang teknik informatika. Memiliki minat besar di bidang pengajaran dan penulisan. Mampu bekerja dalam tim dengan pengalaman
                        organisasi yang cukup.
                    </p>
                    <a href="/about" class="btn btn-about">kenal lebih dekat</a>
                </div>
            </div>
        </div>
    </section>
    <!-- Main Content Ends -->
@endsection
